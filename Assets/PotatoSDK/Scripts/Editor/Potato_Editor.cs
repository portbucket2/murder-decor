using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

namespace PotatoSDK
{
    [CustomEditor(typeof(Potato))]
    public class Potato_Editor : Editor
    {
        const string LOADING_SCREEN_PATH = "Assets/PotatoSDK/Resources/SplashScreen.unity";
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorBuildSettingsScene[] original = EditorBuildSettings.scenes;

            if (original.Length < 1 || !string.Equals(original[0].path, LOADING_SCREEN_PATH))
            {
                if (GUILayout.Button("Add Loading Screen"))
                {
                    AddLoadingScreenNow(original);
                }
            }

        }
        private void OnEnable()
        {
            Prompt();
        }
        private void OnDisable()
        {
            Prompt();
        }
        void Prompt()
        {
            EditorBuildSettingsScene[] original = EditorBuildSettings.scenes;

            Potato p = target as Potato;
            if (p.iDontWantYourLoadingScreen)
            {
                if (original.Length >= 1 && string.Equals(original[0].path, LOADING_SCREEN_PATH))
                {
                    if (EditorUtility.DisplayDialog("Should Potato SDK remove it's Loading Scene!", "You have checked that you dont want the Potato Loading Screen, but it is still added. Should we remove it? ", "Remove", "Keep"))
                    {
                        RemoveLoadingScreenNow(original);
                    }
                    //else
                    //{
                    //    p.iDontWantYourLoadingScreen = false;
                    //    SDKEditorUtilityMan.SetObjectDirty(p);
                    //}
                }
            }
            else
            {
                if (original.Length < 1 || !string.Equals(original[0].path, LOADING_SCREEN_PATH))
                {
                    if (EditorUtility.DisplayDialog("PotatoSDK wants to add it's Loading Scene!", "Potato SDK wants to add an initial Loading Scene to ensure safe initialization. Its recommended that you allow this!", "Allow", "Ignore"))
                    {
                        AddLoadingScreenNow(original);
                    }

                }
            }

        }
        void RemoveLoadingScreenNow(EditorBuildSettingsScene[] original)
        {
            var newSettings = new EditorBuildSettingsScene[original.Length - 1];

            for (int i = 0; i < newSettings.Length; i++)
            {

                newSettings[i] = original[i + 1];

            }
            EditorBuildSettings.scenes = newSettings;
        }
        void AddLoadingScreenNow(EditorBuildSettingsScene[] original)
        {
            var newSettings = new EditorBuildSettingsScene[original.Length + 1];

            for (int i = 0; i < newSettings.Length; i++)
            {
                if (i == 0)
                {
                    newSettings[0] = new EditorBuildSettingsScene(LOADING_SCREEN_PATH, true);
                }
                else
                {
                    newSettings[i] = original[i-1];
                }

            }
            EditorBuildSettings.scenes = newSettings;
        }
    }


}