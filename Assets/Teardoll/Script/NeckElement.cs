using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeckElement : BodyElement
{
    public Transform elementRoot;
    protected override void HandleTear()
    {

        this.transform.GetChild(0).SetParent(elementRoot.parent);

        elementRoot.SetParent(null);
    }
    protected override Transform GetRoot()
    {
        return elementRoot;
    }
}
