using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachToAnimator : MonoBehaviour
{
    public float driveRelax;
    public float drivePull;
    public float driveTorn;
    public Transform targetRoot;
    public Transform animatedRoot;
    public float projectionAngle;
    public float projectionDistance;

    //public Dictionary<ConfigurableJoint, JointData> jointTargetDictionary = new Dictionary<ConfigurableJoint, JointData>();
    public List<JointData> jointList = new List<JointData>();
    // Start is called before the first frame update
    void Awake()
    {
        Recur(targetRoot,animatedRoot);
    }
    
    private void FixedUpdate()
    {
        foreach (var item in jointList)
        {
            if (item.joint==null)
            {
                return;
            }
            float drive = 0;
            switch (item.bodyElement.limbState)
            {
                case LimbState.Relax:
                    drive = driveRelax;
                    SetJoint(item, drive);
                    break;
                case LimbState.Pull:
                    drive = drivePull;
                    SetJoint(item, drive);
                    break;
                case LimbState.Torn:
                    drive = driveTorn;
                    SetJoint(item, drive);
                    break;
            }

        }
    }

    private void SetJoint(JointData item, float drive)
    {
        item.CopyRotation();
        JointDrive jdX = item.joint.angularXDrive;
        jdX.positionSpring = drive;
        item.joint.angularXDrive = jdX;

        JointDrive jdYZ = item.joint.angularYZDrive;
        jdYZ.positionSpring = drive;
        item.joint.angularYZDrive = jdYZ;
        item.joint.projectionMode = JointProjectionMode.PositionAndRotation;
        item.joint.projectionDistance = projectionDistance;
        item.joint.projectionAngle = projectionAngle;
    }


    // Update is called once per frame
    void Recur(Transform ragdollTR, Transform animatedTR)
    {
        BodyElement be = ragdollTR.GetComponent<BodyElement>();
        if(be)
        {
            be.aa = this;
        }

        ConfigurableJoint cj = ragdollTR.GetComponent<ConfigurableJoint>();
        if (cj)
        {
            jointList.Add(new JointData(cj.GetComponent<Rigidbody>(), cj,animatedTR));
        }

        int i = 0;
        foreach (Transform ragdollChild in ragdollTR)
        {
            Transform animatedChild = animatedTR.GetChild(i);
            Recur(ragdollChild,animatedChild);
            i++;
        }
    }
}
[System.Serializable]
public class JointData
{
    public Rigidbody rgbd;
    public ConfigurableJoint joint;
    public Transform target;
    public Quaternion initialRotation;
    public BodyElement bodyElement;
    public JointData() { }
    public JointData(Rigidbody rgbd, ConfigurableJoint joint, Transform target)
    {
        this.rgbd = rgbd;
        this.joint = joint;
        this.target = target;
        initialRotation = target.localRotation;
    }

    public void CopyRotation()
    {
        joint.targetRotation =  Quaternion.Inverse(target.localRotation) * initialRotation;
    }
}