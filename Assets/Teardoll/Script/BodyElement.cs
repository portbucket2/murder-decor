using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyElement : MonoBehaviour
{
    public AttachToAnimator aa;
    public static Dictionary<Rigidbody, BodyElement> inveseRefDictionary = new Dictionary<Rigidbody, BodyElement>();
    public Rigidbody forceEntryBody;

    internal ConfigurableJoint mainJoint;
    public float breakForce;
    public List<Rigidbody> releventBodies;
    public Transform target;

    [Header("Splat")]
    public float radius = 0.15f;
    public int spreadCount = 3;
    public float spreadFactor = 0.45f;
    public int colIndex = 0;
    public bool impactComplete;
    private void Start()
    {
        foreach (Rigidbody item in releventBodies)
        {
            inveseRefDictionary.Add(item, this);
        }
        mainJoint = GetComponent<ConfigurableJoint>();

        foreach (var item in aa.jointList)
        {
            if (releventBodies.Contains(item.rgbd))
            {
                item.bodyElement = this;
            }
        }
    }

    public LimbState limbState;
    public bool torn { get { return limbState == LimbState.Torn; } }
    public void PullStart()
    {
        limbState = LimbState.Pull;
        mainJoint.breakForce = breakForce;
    }
    public void PullRelease()
    {
        limbState = LimbState.Relax;
        mainJoint.breakForce = float.MaxValue;
    }
    void OnJointBreak(float breakForce)
    {
        HandleTear();
        GameStateManager.RequestSwitch(GameState.Torn);
        limbState = LimbState.Torn;
        Debug.Log("A joint has just been broken!, force: " + breakForce);
    }
    protected virtual void HandleTear()
    {
        this.transform.SetParent(null);
    }

    protected virtual Transform GetRoot()
    {
        return this.transform;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (impactComplete) return;
        int l = collision.gameObject.layer;
        if (l == LayerMask.NameToLayer("Paintable"))
        {
            Debug.Log("Found splat point");
            foreach (Rigidbody rgbd in releventBodies)
            {
                RaycastHit rch;
                if (Physics.Raycast(rgbd.position, Vector3.forward, out rch, 100, TexPaintManager.Instance.layerMask))
                {
                    Vector3 rdir = Vector3.forward;
                    TexPaintManager.SplatBurst(
                        colorIndex: colIndex,
                        ray: new Ray(rch.point - rdir * 0.2f, rdir),
                        baseRadius: radius,
                        splatCount: spreadCount,
                        spreadFactor: spreadFactor);
                }
            }


            impactComplete = true;
        }
    }
}
public enum LimbState
{
    Relax = 0,
    Pull = 1,
    Torn = 2,
}