﻿using System.Collections;
using UnityEngine;


public class CopyMotion : MonoBehaviour
{
    [SerializeField] Transform targetLimb;

    ConfigurableJoint configurableJoint;

    Quaternion targetInitialRotaiton;


    // Use this for initialization
    void Start()
    {
        configurableJoint = GetComponent<ConfigurableJoint>();
        targetInitialRotaiton = targetLimb.localRotation;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        configurableJoint.targetRotation = CopyRotation();
    }

    private Quaternion CopyRotation()
    {
        return Quaternion.Inverse(targetLimb.localRotation) * targetInitialRotaiton;
    }
}
