using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class DragMotion : MonoBehaviour
{
    public float xzVel = 10;
    public float accMag = 100;
    DragReporter dr;
    Joint dragPoint;
    Rigidbody rgbd;
    // Start is called before the first frame update
    void Start()
    {
        rgbd = GetComponent<Rigidbody>();
        dragPoint = GetComponent<Joint>();
        dr = GetComponent<DragReporter>();
        dr.onDragStart += OnDragStart;
        dr.onDrag += OnDrag;
        dr.onDragEnd += OnDragEnd;
    }
    public Camera cam;
    public LayerMask dragPlaneLayer;
    public LayerMask elementLayer;
    public float forceMult= 5;


    BodyElement draggableElement;
    public Vector3 lastDragPoint;


    void OnDragStart(Vector2 v)
    {
        if (GameStateManager.State == GameState.Idle)
        {
            lastDragPoint = GetPointOnDraggablePlane();
            dragPoint.transform.position = lastDragPoint;

            RaycastHit rch;
            Ray r = cam.ScreenPointToRay(new Vector3(v.x, v.y, 0));
            if (Physics.Raycast(r, out rch, 100, elementLayer))
            {
                Debug.Log(rch.collider.gameObject);
                draggableElement = BodyElement.inveseRefDictionary[rch.collider.attachedRigidbody];
                draggableElement.PullStart();
                dragPoint.connectedBody = draggableElement.forceEntryBody;
                GameStateManager.RequestSwitch(GameState.Pull);
            }
        } 
    }
    void OnDrag(Vector2 v)
    {
        if (!GameStateManager.inThrow)
        {
            Vector3 newPoint = GetPointOnDraggablePlane();
            dragPoint.transform.position = newPoint;
        } 
    }
    void OnDragEnd(Vector2 v)
    {
        if (draggableElement)
        {
            if (!draggableElement.torn)
            {
                dragPoint.connectedBody = null;
                draggableElement.PullRelease();

                GameStateManager.RequestSwitch(GameState.Idle);
            }
            else
            {
                StartCoroutine(ThrowRoutine(draggableElement));
                GameStateManager.RequestSwitch(GameState.Throw);
            }

        }
        draggableElement = null;
    }

    Vector3 GetPointOnDraggablePlane()
    {
        RaycastHit rch;
        Ray r = cam.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(r, out rch, 100, dragPlaneLayer))
        {
            Debug.DrawLine(r.origin, rch.point);
            return rch.point;
        }
        Debug.LogError("plane not found");
        return Vector3.zero;
    }

    IEnumerator ThrowRoutine(BodyElement be)
    {
        Vector3 direction = be.target.position - rgbd.position;

        //rgbd.isKinematic = false;
        //rgbd.useGravity = true;
        //rgbd.velocity = Vector3.zero;
        //rgbd.AddForce(direction * forceMult);

        Vector3 xzDir = direction;
        xzDir.y = 0;
        float Vy = xzVel;
        while (!be.impactComplete)
        {
            Vy = Vy -9.8f * Time.deltaTime;

            Vector3 disp = new Vector3(0, Vy, 0) + xzDir*xzVel;

            this.transform.position = this.transform.position + disp*Time.deltaTime;

            yield return null;
        }
        dragPoint.connectedBody = null;
        GameStateManager.RequestSwitch(GameState.Idle);

    }
}
