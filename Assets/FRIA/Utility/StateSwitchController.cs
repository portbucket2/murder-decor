﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class StateSwitchController<T>
{
    public T currentState
    {
        get 
        {
            return _current;
        }
    }

    private T _current;

    private Dictionary<int, List<GameObject>> switchBoard = new Dictionary<int, List<GameObject>>();


    public StateSwitchController()
    {
        _current = default(T);
    }
    public void AddState(T state, List<GameObject> activeList)
    {
        int si = (int)Convert.ChangeType(state, typeof(int));
        switchBoard.Add(si, activeList);
    }

    public void SwtichState(T state)
    {
        _current = state;

        int si = (int) Convert.ChangeType(state, typeof(int));
        foreach (KeyValuePair<int,List<GameObject>> kvp in switchBoard)
        {
            if ( kvp.Key != si)
            {
                foreach (var gameobject in kvp.Value)
                {
                    gameobject.SetActive(false);
                }
            }
        }
        foreach (var item in switchBoard[si])
        {
            item.SetActive(true);
        }
    }
}