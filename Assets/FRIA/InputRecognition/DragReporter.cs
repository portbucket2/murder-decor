﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragReporter : MonoBehaviour
{
    const float defWidth = 900;
    public float defPixCount = 15;
    public event System.Action<Vector2> onDragStart;
    public event System.Action<Vector2> onDrag;
    public event System.Action<Vector2> onDragEnd;
    public event System.Action<Vector3> onTap;
    float minimalPixelCount;

    public static DragReporter instance;
    public Drag currentDrag = null;
    private void Start()
    {
        instance = this;
        minimalPixelCount =  (defPixCount/defWidth)* Screen.width;
    }
    private void FixedUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            currentDrag = new Drag(new Vector2(Input.mousePosition.x, Input.mousePosition.y), minimalPixelCount);
            currentDrag.onDragConfirmed += onDragStart;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (currentDrag != null)
            {
                if (currentDrag.dragConfrimed)
                {
                    onDragEnd?.Invoke(Input.mousePosition);
                }
                else
                {
                    onTap?.Invoke(Input.mousePosition);
                }
            }
            currentDrag = null;
        }

        if (currentDrag != null)
        {
            bool dragWasPending = !currentDrag.dragConfrimed;
            Vector2 v = currentDrag.GetDragAmount(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
            if(!dragWasPending) onDrag?.Invoke(v);
        }
    }
}

public class Drag
{
    public event System.Action<Vector2> onDragConfirmed;
    Vector2 startPos;
    Vector2 lastPos;
    public bool dragConfrimed { get; private set; }
    public Vector2 travelVec { get { return lastPos - startPos; } }
    float startTime;
    float minPixel = 100;

    public bool didntDragFarEnough
    {
        get
        {
            return travelVec.magnitude < minPixel;
        }
    }
    public Drag(Vector2 startPosition, float minPix)
    {
        this.minPixel = minPix;
        startTime = Time.time;
        startPos = startPosition;
        lastPos = startPosition;
    }
    public Vector2 GetDragAmount(Vector2 currentPosition)
    {
        lastPos = currentPosition;
        //Debug.Log(travelVec);

        if (didntDragFarEnough)
        {
            return Vector2.zero;
        }
        else
        {
            if(!dragConfrimed)
            {
                dragConfrimed = true;
                onDragConfirmed?.Invoke(startPos);
            }
            return travelVec;
        }
    }
}
