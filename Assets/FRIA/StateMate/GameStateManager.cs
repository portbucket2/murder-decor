﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class GameStateManager : MonoBehaviour
{
    public static GameStateManager Instance { get; private set; }
    public static IStateMateAccess<GameState> stateAccess { get { return Instance.stateMate; } }
    public static GameState State { get { return stateAccess.CurrentState; } }
    public static bool inThrow { get { return State == GameState.Throw; } }
    
    private StateMate<GameState> stateMate;

    public List<GameObject> startObjs;
    public List<GameObject> purchaseObjs;
    public List<GameObject> inGameObjs;
    private void Awake()
    {
        Instance = this;
        stateMate = new StateMate<GameState>(initial: GameState.Idle, enableDefinedTransitionOnly: true, loadEmpties: true);

        stateMate.AddTransition(GameState.Idle, GameState.Pull);
        stateMate.AddTransition(GameState.Pull, GameState.Torn);
        stateMate.AddTransition(GameState.Pull, GameState.Idle);
        stateMate.AddTransition(GameState.Torn, GameState.Throw);
        stateMate.AddTransition(GameState.Throw, GameState.Idle);
        stateMate.logStateSwitches = true;

    }
    private void Start()
    {
        stateMate.SetInitialSwitches();
    }
    public static void RequestSwitch(GameState gs)
    {
        Instance.stateMate.SwitchState(gs);
    }
}

public enum GameState
{
    Idle = 0,
    Pull = 1,
    Torn = 2,
    Throw = 3,
}