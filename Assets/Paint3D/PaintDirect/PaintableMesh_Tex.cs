﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;

public class PaintableMesh_Tex : PaintableMesh
{
    public const int MAX_COL = 8;
    public int listCapacity = 200;
    public float worldToPix = 100;
    public float innerRatio = 0.8f;
    [Range(1, 10)]
    public int TexBits = 9;

    public bool multiVert = false;

    Texture2D splatMap0;
    Color[] splatArray0;
    Texture2D splatMap1;
    Color[] splatArray1;
    SuperVector[] superSplatArray;
    
    Vector2[] uvData;
    int N;

    public override void OnInit()
    {
        uvData = mesh.uv;
        N = Mathf.ClosestPowerOfTwo((int) Mathf.Pow(2 , TexBits));
        splatMap0 = new Texture2D(N, N);
        splatMap1 = new Texture2D(N, N);
        splatArray0 = new Color[N * N];
        splatArray1 = new Color[N * N];
        superSplatArray = new SuperVector[N * N];
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                splatArray0[i * N + j] = new Color(0,0,0,0);
                splatArray1[i * N + j] = new Color(0, 0, 0, 0);
                superSplatArray[i * N + j] = new SuperVector(MAX_COL);
            }
        }
        splatMap0.SetPixels(splatArray0);
        splatMap1.SetPixels(splatArray1);
        splatMap0.Apply();
        splatMap1.Apply();
        mat.SetTexture("_SplatMap0", splatMap0);
        mat.SetTexture("_SplatMap1", splatMap1);
        indicesInRange = new List<int>(listCapacity);
    }


    [StructLayout(LayoutKind.Explicit)]
    public struct intfloatunion
    {
        [FieldOffset(0)]
        public float f;
        [FieldOffset(0)]
        public int i;
    }
    intfloatunion ifu = new intfloatunion();


    List<int> indicesInRange;
    public override void OnColorHit(RaycastHit rch, float radius, int colorIndex, float lerpRate)
    {
        Vector3 hitPos = this.transform.InverseTransformPoint(rch.point);
        //Vector3 distVec;
        float sqrMag;
        indicesInRange.Clear();


        float x, y, z;
        float closestSqrMag = float.MaxValue;
        for (int i = 0; i < verts.Length; i++)
        {
            x = verts[i].x - hitPos.x;
            y = verts[i].y - hitPos.y;
            z = verts[i].z - hitPos.z;

            sqrMag = x*x + y*y +z*z;
            if (multiVert)
            {
                if (radius*radius > sqrMag)
                {
                    indicesInRange.Add(i);
                    //weights.Add(1- (mag/radius));
#if UNITY_EDITOR
                    if (indicesInRange.Count > listCapacity)
                    {
                        //Debug.LogErrorFormat("Capacity is lower {0}/{1}", indicesInRange.Count, listCapacity);
                    }
#endif
                }
            }
            else
            {
                if (sqrMag < closestSqrMag)
                {
                    if (indicesInRange.Count == 0) indicesInRange.Add(i);
                    else indicesInRange[0] = i;
                    closestSqrMag = sqrMag;
                }
            }
        }


        float count1D = Mathf.Sqrt(indicesInRange.Count*4/Mathf.PI);

        

        float deltaTime = Time.deltaTime;
        float old_a;
        float sigmoid_R;


        Stage2(colorIndex, radius, count1D) ;
        Stage3();

        //Debug.Log(indicesInRange.Count);

        splatMap0.SetPixels(splatArray0);
        splatMap1.SetPixels(splatArray1);
        splatMap0.Apply();
        splatMap1.Apply();
    }

    int u;
    int v;

    int sqrPixDist;
    float pixDist;
    public void Stage2(int colorIndex,float radius, float count1D)
    {
        int preInt = (1 << 29);
        int postInt = (1 << 22);

        float r_io=0;
        float pixRadius = worldToPix * radius * 2 / count1D;
        //Debug.LogFormat("{0}  {1}",pixRadius, indicesInRange.Count);
        float pixRadiusInner = pixRadius * innerRatio;
        bool shouldAssign;
        int assignIndex=-1;

        float pixRadOut = (pixRadius - pixRadiusInner);
        float pixRadSq_Inner = pixRadiusInner*pixRadiusInner;
        float pixRadSqr = pixRadius * pixRadius;
        for (int ind_i = 0; ind_i < indicesInRange.Count; ind_i++)
        {
            int i = indicesInRange[ind_i];
            u = (int) (uvData[i].x * N);
            v = (int) (uvData[i].y * N);
            for (int r = 0; r < N; r++)
            {
                for (int c = 0; c < N; c++)
                {
                    sqrPixDist = (u - c) * (u - c) + (v - r) * (v - r);
                    if (sqrPixDist < pixRadSq_Inner)
                    {
                        r_io = 1;
                        assignIndex = r * N + c;
                    }
                    else if (sqrPixDist < pixRadSqr)
                    {
                        ifu.i = 0;
                        ifu.f = (u - c) * (u - c) + (v - r) * (v - r);
                        ifu.i = preInt + (ifu.i >> 1) - postInt;
                        pixDist = ifu.f;

                        r_io = 1 - ((pixDist - pixRadiusInner) / pixRadOut);

                        assignIndex = r * N + c;


                    }
                    else
                    {
                        assignIndex = -1;
                        r_io = 0;
                    }
                    if (assignIndex >= 0)
                    {
                        if (superSplatArray[assignIndex].vals[colorIndex] < r_io)
                        {
                            if (r_io >= 1)
                            {
                                for (int ci = 0; ci < superSplatArray[assignIndex].vals.Length; ci++)
                                {
                                    superSplatArray[assignIndex].vals[ci] = 0;
                                }
                                superSplatArray[assignIndex].vals[colorIndex] = 1;
                            }
                            else //if(colorIndex == 0)
                            {
                                prevTotal = 0;
                                oldValI = superSplatArray[assignIndex].vals[colorIndex];
                                for (int ci = 0; ci < superSplatArray[assignIndex].vals.Length; ci++)
                                {
                                    prevTotal += superSplatArray[assignIndex].vals[ci];
                                }

                                prevTotal -= oldValI;
                                
                                spacingMult = 1 - r_io;
                                if (prevTotal > 0)
                                {
                                    for (int ci = 0; ci < superSplatArray[assignIndex].vals.Length; ci++)
                                    {
                                        superSplatArray[assignIndex].vals[ci] = superSplatArray[assignIndex].vals[ci] * spacingMult / prevTotal;
                                    }
                                }
                                superSplatArray[assignIndex].vals[colorIndex] = r_io;
                            }
                        }
                    }

                }
            }
        }
    }
    public void Stage3()
    {
        int index = 0;
        for (int r = 0; r < N; r++)
        {
            for (int c = 0; c < N; c++)
            {
                index = r * N + c;
                splatArray0[index].r = superSplatArray[index].vals[0];
                splatArray0[index].g = superSplatArray[index].vals[1];
                splatArray0[index].b = superSplatArray[index].vals[2];
                splatArray0[index].a = superSplatArray[index].vals[3];

                splatArray1[index].r = superSplatArray[index].vals[4];
                splatArray1[index].g = superSplatArray[index].vals[5];
                splatArray1[index].b = superSplatArray[index].vals[6];
                splatArray1[index].a = superSplatArray[index].vals[7];
            }
        }
    }


    static float spacingMult;
    static float prevTotal;
    static float oldValI;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Assign(ref float[] vals, int coloringIndex, float calculatedValue)
    {
        if (vals[coloringIndex] < calculatedValue)
        {
            if (calculatedValue >= 1)
            {
                for (int ci = 0; ci < vals.Length; ci++)
                {
                    vals[ci] = 0;
                }
                vals[coloringIndex] = 1;
            }
            else
            {
                prevTotal = 0;
                oldValI = vals[coloringIndex];
                for (int ci = 0; ci < vals.Length; ci++)
                {
                    prevTotal += vals[ci];
                }
                prevTotal -= oldValI;

                spacingMult = 1 - calculatedValue;

                for (int ci = 0; ci < vals.Length; ci++)
                {
                    vals[ci] = vals[ci] * spacingMult / prevTotal;
                }
                vals[coloringIndex] = calculatedValue;
            }
        }


    }
}
public struct SuperVector
{
    public float[] vals;
    public SuperVector(int count)
    {
        vals = new float[count];
        for (int i = 0; i < count; i++)
        {
            vals[i] = 0;
        }
    }

}