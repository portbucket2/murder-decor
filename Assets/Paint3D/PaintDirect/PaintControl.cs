﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PaintControl : MonoBehaviour
{
    public float radius = 1;
    public int colorIndex;
    public List<Color> cols;
    Camera cam;

    public List<Button> buttonsToChange;
    // Start is called before the first frame update
    void Awake()
    {
        cam = Camera.main;

        foreach (var item in FindObjectsOfType<PaintableMesh>())
        {
            item.Init(cols);
        }


        for (int i = 0; i < cols.Count; i++)
        {
            int index = i;
            buttonsToChange[i].GetComponent<Image>().color = cols[i];
            buttonsToChange[i].onClick.AddListener(() => { colorIndex = index; });
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < 8; i++)
        {
            if (Input.GetKeyDown((KeyCode)(i + 256)))
            {
                colorIndex = i;
            }
        }

        if (Input.GetMouseButton(0))
        {
            Ray r = cam.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
            RaycastHit rch;
            if (Physics.Raycast(r, out rch, 100))
            {
                PaintableMesh pm = rch.collider.gameObject.GetComponentInParent<PaintableMesh>();
                if(pm) pm.OnColorHit(rch, radius, colorIndex, 1);
            }
        }
    }
}