﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PaintableMesh : MonoBehaviour
{
    public MeshRenderer mrenderer;
    public MeshFilter mfilter;
    public Mesh mesh;


    protected Vector3[] verts;
    protected Material mat;
    // Start is called before the first frame update
    public void Init(List<Color> cols)
    {

        mrenderer = this.GetComponent<MeshRenderer>();
        mat = mrenderer.sharedMaterial;// Instantiate(mrenderer.material);
        //mrenderer.material = mat;

        mfilter = this.GetComponent<MeshFilter>();
        mesh = Instantiate(mfilter.mesh);
        mfilter.mesh = mesh;

        verts = mesh.vertices;


        for (int i = 0; i < cols.Count; i++)
        {
            if (i < 8)
            {
                mat.SetColor(string.Format("_Color{0}",i),cols[i]);
            }
        }

        OnInit();
    }
    public abstract void OnInit();

    public abstract void OnColorHit(RaycastHit rch, float radius, int colorIndex, float lerpRate);
}
