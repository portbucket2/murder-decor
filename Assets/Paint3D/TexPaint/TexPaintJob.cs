﻿using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;


[BurstCompile]
public struct TexPaintJob : IJobParallelFor
{

    [ReadOnly] public NativeArray<int> params_int;
    [ReadOnly] public NativeArray<float> params_float;
    //public NativeArray<float3> verts;
    //[ReadOnly] public NativeArray<float2> uvData;

    [ReadOnly] public NativeArray<float3> uvHitsAndRadius;


    [NativeDisableContainerSafetyRestriction] public NativeArray<float4> splatArray0;
    void IJobParallelFor.Execute(int r)
    {
        int indicesInRangeCount = params_int[3];
        if (indicesInRangeCount > 0)
        {
            int colorCount = 4;
            int colorIndex = params_int[1];
            int N = params_int[2];
            //float count1D = math.sqrt(indicesInRangeCount * 4 / math.PI);

            float worldToPix = params_float[0];
            float innerRatio = params_float[1];

            float pixRadius;
            float pixRadiusInner;
            float pixRadOut;

            float pixRadSqr;
            float pixRadSq_Inner;

            int u, v;
            float sqrPixDist, pixDist;
            float spacingMult, prevTotal, oldValI;
            float r_io;
            float2 uvd;

            NativeArray<float> pixVals = new NativeArray<float>(4, Allocator.Temp);
            int pixIndex = 0;
            bool skipPixel;
            float targetPixVal;
            float4 color0;
            float3 currentHitPointData;
            for (int c = 0; c < N; c++)
            {
                pixIndex = r * N + c;
                color0 = splatArray0[pixIndex];

                pixVals[0] = color0.x;
                pixVals[1] = color0.y;
                pixVals[2] = color0.z;
                pixVals[3] = color0.w;


                skipPixel = true;
                targetPixVal = pixVals[colorIndex];

                //int temp_i;
                for (int ind_i = 0; ind_i < indicesInRangeCount; ind_i++)
                {
                    currentHitPointData = uvHitsAndRadius[ind_i];







                    pixRadius = worldToPix * currentHitPointData.z;
                    pixRadiusInner = innerRatio * pixRadius;
                    pixRadOut = (pixRadius - pixRadiusInner);

                    pixRadSqr = pixRadius * pixRadius;
                    pixRadSq_Inner = pixRadiusInner * pixRadiusInner;

                    //temp_i = (int)uvHitsAndRadius[ind_i].x;
                    //uvd = uvData[temp_i];
                    uvd.x = uvHitsAndRadius[ind_i].x;
                    uvd.y = uvHitsAndRadius[ind_i].y;
                    u = (int)(uvd.x * N);
                    v = (int)(uvd.y * N);

                    sqrPixDist = (u - c) * (u - c) + (v - r) * (v - r);

                    if (sqrPixDist < pixRadSq_Inner)
                    {
                        r_io = 1;
                        skipPixel = false;
                    }
                    else if (sqrPixDist < pixRadSqr)
                    {
                        pixDist = math.sqrt((u - c) * (u - c) + (v - r) * (v - r));
                        r_io = 1 - ((pixDist - pixRadiusInner) / pixRadOut);
                        skipPixel = false;
                    }
                    else
                    {
                        r_io = 0;
                    }

                    if (r_io > targetPixVal)
                    {
                        targetPixVal = r_io;
                    }
                }


                if (!skipPixel)
                {


                    if (pixVals[colorIndex] < targetPixVal)
                    {
                        if (targetPixVal >= 1)
                        {
                            for (int ci = 0; ci < colorCount; ci++)
                            {
                                pixVals[ci] = 0;
                            }
                            pixVals[colorIndex] = 1;
                        }
                        else
                        {
                            prevTotal = 0;
                            oldValI = pixVals[colorIndex];
                            for (int ci = 0; ci < colorCount; ci++)
                            {
                                prevTotal += pixVals[ci];
                            }
                            prevTotal -= oldValI;

                            spacingMult = 1 - targetPixVal;
                            if (prevTotal > 0)
                            {
                                for (int ci = 0; ci < colorCount; ci++)
                                {
                                    pixVals[ci] = pixVals[ci] * spacingMult / prevTotal;
                                }
                            }
                            pixVals[colorIndex] = targetPixVal;
                        }
                    }
                }


                color0.x = pixVals[0];
                color0.y = pixVals[1];
                color0.z = pixVals[2];
                color0.w = pixVals[3];


                splatArray0[pixIndex] = color0;

            }

            pixVals.Dispose();
        }


        //params_int[1]++;




    }
}
