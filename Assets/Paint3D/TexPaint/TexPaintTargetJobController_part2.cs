﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;

public partial class  TexPaintTargetJobController 
{

    // public const int MAX_COL = 8;
    // public int indexListCapacity = 1000;
    [Range(-1,90)]
    public float calculateProgressUpTo_percentage = -1;
    public float worldToPix_256 = 18;
    private float innerRatio = 0.85f;

    public int TexResolution = 256;

    private float worldToPix = 20;
    //public bool multiVert = false;

    private Renderer mrenderer;
    private MeshFilter mfilter;
    private Mesh mesh;

    private NativeArray<float3> verts;
    private NativeArray<float2> uvData;
    private NativeArray<float4> splatFloatArray0;
    private NativeArray<float4> splatFloatArray1;
    private NativeArray<float2> indicesInRange;
    private NativeArray<float> paramsFloat;
    private NativeArray<int> paramsInt;
    private NativeArray<float> pixelProgresses;

    public float[] pixProgressArray;

    private float4[] copyArray0;
    private float4[] copyArray1;

    private Material mat;
    private Texture2D splatMap0;
    private Color[] splatArray0;

    public int N { get; private set; }

    float lengthMult;
    int vCount;

    bool skinned = false;
    //public MeshFilter debugMeshRenderer;
    public void Init(List<Color> cols)
    {
        lengthMult = this.transform.InverseTransformVector(Vector3.up).magnitude;
        //Debug.LogFormat("length mult = {0}", lengthMult);
        worldToPix = worldToPix_256 * TexResolution / 256.0f;
        mrenderer = this.GetComponent<MeshRenderer>();
        if (!mrenderer)
        {
            skinned = true;
            mrenderer = this.GetComponent<SkinnedMeshRenderer>();
            mesh = new Mesh();
            (mrenderer as SkinnedMeshRenderer).BakeMesh(mesh);
            //if(debugMeshRenderer)debugMeshRenderer.mesh = mesh;
        }
        mat = Instantiate(mrenderer.material);
        mrenderer.material = mat;
        for (int i = 0; i < cols.Count; i++)
        {
            if (i < 8)
            {
                mat.SetColor(string.Format("_Color{0}", i), cols[i]);
            }
        }

        if (!skinned)
        {
            mfilter = this.GetComponent<MeshFilter>();
            mesh = Instantiate(mfilter.mesh);
            mfilter.mesh = mesh;
        }


        Vector3[] tempVx = mesh.vertices;
        Vector2[] tempUV = mesh.uv;
        vCount = mesh.vertices.Length;

        verts = new NativeArray<float3>(vCount, Allocator.Persistent);
        uvData = new NativeArray<float2>(vCount, Allocator.Persistent);
        indicesInRange = new NativeArray<float2>(vCount, Allocator.Persistent);

        paramsFloat = new NativeArray<float>(20, Allocator.Persistent);
        paramsInt = new NativeArray<int>(20, Allocator.Persistent);

        for (int i = 0; i < vCount; i++)
        {
            verts[i] = tempVx[i];
            uvData[i] = tempUV[i];
        }


        N = TexResolution;
        splatMap0 = new Texture2D(N, N);

        splatArray0 = new Color[N * N];
        splatFloatArray0 = new NativeArray<float4>(N * N, Allocator.Persistent);
        splatFloatArray1 = new NativeArray<float4>(N * N, Allocator.Persistent);
        pixelProgresses = new NativeArray<float>(8,Allocator.Persistent);
        pixProgressArray = new float[8];
        copyArray0 = new float4[N * N];
        copyArray1 = new float4[N * N];
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                splatArray0[i * N + j] = new Color(0, 0, 0, 0);
            }
        }
        splatMap0.SetPixels(splatArray0);
        splatMap0.Apply();
        mat.SetTexture("_SplatMap0", splatMap0);

        ResetHittionary();
    }

    void OnDestroy()
    {
        FinishCurrentJobs();
        pixelProgresses.Dispose();
        verts.Dispose();
        uvData.Dispose();
        indicesInRange.Dispose();
        splatFloatArray0.Dispose();
        splatFloatArray1.Dispose();
        paramsFloat.Dispose();
        paramsInt.Dispose();
        for (int i = hitArrayList.Count - 1; i >= 0; i--)
        {
            hitArrayList[i].Dispose();
            hitArrayList.RemoveAt(i);
        }
    }


    List<NativeArray<float3>> hitArrayList = new List<NativeArray<float3>>();
    public void ScheduleRecolor()
    {
        paramsInt[0] = 8;
        paramsInt[1] = 0;
        paramsInt[2] = N;
        paramsFloat[0] = worldToPix;
        paramsFloat[1] = innerRatio;
        paramsFloat[10] = lengthMult;
        paramsFloat[15] = N*N*calculateProgressUpTo_percentage/100;
        totalHandle = new JobHandle();

        if (inkDots.Count>0)
        {
            paramsInt[3] = inkDots.Count;//passed through previous job
            NativeArray<float3> hits = new NativeArray<float3>(inkDots.Count, Allocator.TempJob);
            for (int i = 0; i < hits.Length; i++)
            {
                hits[i] = inkDots[i];
            }

            hitArrayList.Add(hits);

            TexPaintJob pj = new TexPaintJob();
            pj.uvHitsAndRadius = hits;
            pj.splatArray0 = splatFloatArray0;
            pj.params_int = paramsInt;
            pj.params_float = paramsFloat;

            totalHandle = pj.Schedule(N, 1, totalHandle);
        }

        scheduled = true;
        ResetHittionary();
    }


    public JobHandle totalHandle;
    bool scheduled = false;

    public void FinishCurrentJobs()
    {
        if (scheduled)
        {
            scheduled = false;
            totalHandle.Complete();
            ApplyJobResult();
        }
    }
    public void ApplyJobResult()
    {
        if (calculateProgressUpTo_percentage > 0) pixelProgresses.CopyTo(pixProgressArray);
        int index;
        splatFloatArray0.CopyTo(copyArray0);
        for (int r = 0; r < N; r++)
        {
            for (int c = 0; c < N; c++)
            {
                index = r * N + c;
                splatArray0[index].r = copyArray0[index].x;
                splatArray0[index].g = copyArray0[index].y;
                splatArray0[index].b = copyArray0[index].z;
                splatArray0[index].a = copyArray0[index].w;
            }
        }

        splatMap0.SetPixels(splatArray0);
        splatMap0.Apply();

        for (int i = hitArrayList.Count - 1; i >= 0; i--)
        {
            hitArrayList[i].Dispose();
            hitArrayList.RemoveAt(i);
        }
        //Debug.Break();
    }
}
