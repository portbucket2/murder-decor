﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;

public partial class  SplatTargetJobController 
{

    // public const int MAX_COL = 8;
    // public int indexListCapacity = 1000;
    [Range(-1,90)]
    public float calculateProgressUpTo_percentage = -1;
    public float worldToPix_256 = 18;
    private float innerRatio = 0.85f;

    public int TexResolution = 256;

    private float worldToPix = 20;
    //public bool multiVert = false;

    private Renderer mrenderer;
    private MeshFilter mfilter;
    private Mesh mesh;

    private NativeArray<float3> verts;
    private NativeArray<float2> uvData;
    private NativeArray<float4> splatFloatArray0;
    private NativeArray<float4> splatFloatArray1;
    private NativeArray<float2> indicesInRange;
    private NativeArray<float> paramsFloat;
    private NativeArray<int> paramsInt;
    private NativeArray<float> pixelProgresses;

    public float[] pixProgressArray;

    private float4[] copyArray0;
    private float4[] copyArray1;

    private Material mat;
    private Texture2D splatMap0;
    private Color[] splatArray0;
    private Texture2D splatMap1;
    private Color[] splatArray1;

    public int N { get; private set; }

    float lengthMult;
    int vCount;

    bool skinned = false;
    //public MeshFilter debugMeshRenderer;
    public void Init(List<Color> cols)
    {
        lengthMult = this.transform.InverseTransformVector(Vector3.up).magnitude;
        //Debug.LogFormat("length mult = {0}", lengthMult);
        worldToPix = worldToPix_256 * TexResolution / 256.0f;
        mrenderer = this.GetComponent<MeshRenderer>();
        if (!mrenderer)
        {
            skinned = true;
            mrenderer = this.GetComponent<SkinnedMeshRenderer>();
            mesh = new Mesh();
            (mrenderer as SkinnedMeshRenderer).BakeMesh(mesh);
            //if(debugMeshRenderer)debugMeshRenderer.mesh = mesh;
        }
        mat = Instantiate(mrenderer.material);
        mrenderer.material = mat;
        for (int i = 0; i < cols.Count; i++)
        {
            if (i < 8)
            {
                mat.SetColor(string.Format("_Color{0}", i), cols[i]);
            }
        }

        if (!skinned)
        {
            mfilter = this.GetComponent<MeshFilter>();
            mesh = Instantiate(mfilter.mesh);
            mfilter.mesh = mesh;
        }


        Vector3[] tempVx = mesh.vertices;
        Vector2[] tempUV = mesh.uv;
        vCount = mesh.vertices.Length;

        verts = new NativeArray<float3>(vCount, Allocator.Persistent);
        uvData = new NativeArray<float2>(vCount, Allocator.Persistent);
        indicesInRange = new NativeArray<float2>(vCount, Allocator.Persistent);

        paramsFloat = new NativeArray<float>(20, Allocator.Persistent);
        paramsInt = new NativeArray<int>(20, Allocator.Persistent);

        for (int i = 0; i < vCount; i++)
        {
            verts[i] = tempVx[i];
            uvData[i] = tempUV[i];
        }


        N = TexResolution;
        splatMap0 = new Texture2D(N, N);
        splatMap1 = new Texture2D(N, N);

        splatArray0 = new Color[N * N];
        splatArray1 = new Color[N * N];
        splatFloatArray0 = new NativeArray<float4>(N * N, Allocator.Persistent);
        splatFloatArray1 = new NativeArray<float4>(N * N, Allocator.Persistent);
        pixelProgresses = new NativeArray<float>(8,Allocator.Persistent);
        pixProgressArray = new float[8];
        copyArray0 = new float4[N * N];
        copyArray1 = new float4[N * N];
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                splatArray0[i * N + j] = new Color(0, 0, 0, 0);
                splatArray1[i * N + j] = new Color(0, 0, 0, 0);
            }
        }
        splatMap0.SetPixels(splatArray0);
        splatMap1.SetPixels(splatArray1);
        splatMap0.Apply();
        splatMap1.Apply();
        mat.SetTexture("_SplatMap0", splatMap0);
        mat.SetTexture("_SplatMap1", splatMap1);

        ResetHittionary();
    }

    void OnDestroy()
    {
        FinishCurrentJobs();
        pixelProgresses.Dispose();
        verts.Dispose();
        uvData.Dispose();
        indicesInRange.Dispose();
        splatFloatArray0.Dispose();
        splatFloatArray1.Dispose();
        paramsFloat.Dispose();
        paramsInt.Dispose();
        for (int i = hitArrayList.Count - 1; i >= 0; i--)
        {
            hitArrayList[i].Dispose();
            hitArrayList.RemoveAt(i);
        }
    }


    List<NativeArray<float4>> hitArrayList = new List<NativeArray<float4>>();
    public void ScheduleRecolor()
    {
        paramsInt[0] = 8;
        paramsInt[1] = 0;
        paramsInt[2] = N;
        //paramsInt[3] = indicesInRangeCount;//passed through previous job
        paramsFloat[0] = worldToPix;
        paramsFloat[1] = innerRatio;
        paramsFloat[10] = lengthMult;
        paramsFloat[15] = N*N*calculateProgressUpTo_percentage/100;
        totalHandle = new JobHandle();

        foreach (KeyValuePair<int,List<float4>> kvp in hittionary)
        {
            int color_i = kvp.Key;
            List<float4> hitlist = kvp.Value;
            if (hitlist.Count > 0)
            {
                NativeArray<float4> hits = new NativeArray<float4>(hitlist.Count,Allocator.TempJob);
                for (int i = 0; i < hits.Length; i++)
                {
                    hits[i] = hitlist[i];
                }
                hitArrayList.Add(hits);

                SearchVertJob svj = new SearchVertJob();
                svj.verts = verts;
                svj.indicesInRange = indicesInRange;
                svj.params_int = paramsInt;
                svj.params_float = paramsFloat;
                svj.paintPoints = hits;


                PaintJob pj = new PaintJob();
                pj.indicesInRange = indicesInRange;
                pj.uvData = uvData;
                pj.splatArray0 = splatFloatArray0;
                pj.splatArray1 = splatFloatArray1;
                pj.params_int = paramsInt;
                pj.params_float = paramsFloat;



                totalHandle = svj.Schedule(totalHandle);
                totalHandle = pj.Schedule(N, 1, totalHandle);
            }
            IncrementIndexJob ij = new IncrementIndexJob();
            ij.params_int = paramsInt;
            totalHandle = ij.Schedule(totalHandle);
        }
        if (calculateProgressUpTo_percentage > 0)
        {
            ProgJob prj = new ProgJob();
            prj.params_int = paramsInt;
            prj.params_float = paramsFloat;
            prj.splatArray0 = splatFloatArray0;
            prj.splatArray1 = splatFloatArray1;
            prj.vals = pixelProgresses;
            totalHandle = prj.Schedule(totalHandle);
        }


        scheduled = true;
        ResetHittionary();
    }


    public JobHandle totalHandle;
    bool scheduled = false;

    public void FinishCurrentJobs()
    {
        if (scheduled)
        {
            scheduled = false;
            totalHandle.Complete();
            ApplyJobResult();
        }
    }
    public void ApplyJobResult()
    {

        //Debug.Log(paramsInt[3] + " " + Time.time);
        if (calculateProgressUpTo_percentage > 0) pixelProgresses.CopyTo(pixProgressArray);
        int index;
        splatFloatArray0.CopyTo(copyArray0);
        //splatFloatArray1.CopyTo(copyArray1);
        for (int r = 0; r < N; r++)
        {
            for (int c = 0; c < N; c++)
            {
                index = r * N + c;
                splatArray0[index].r = copyArray0[index].x;
                splatArray0[index].g = copyArray0[index].y;
                splatArray0[index].b = copyArray0[index].z;
                splatArray0[index].a = copyArray0[index].w;

                //splatArray1[index].r = copyArray1[index].x;
                //splatArray1[index].g = copyArray1[index].y;
                //splatArray1[index].b = copyArray1[index].z;
                //splatArray1[index].a = copyArray1[index].w;
            }
        }

        splatMap0.SetPixels(splatArray0);
        //splatMap1.SetPixels(splatArray1);
        splatMap0.Apply();
        //splatMap1.Apply();

        for (int i = hitArrayList.Count - 1; i >= 0; i--)
        {
            hitArrayList[i].Dispose();
            hitArrayList.RemoveAt(i);
        }
        //Debug.Break();
    }
}
