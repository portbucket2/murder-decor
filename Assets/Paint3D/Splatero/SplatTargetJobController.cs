﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;

public partial class SplatTargetJobController : MonoBehaviour
{

    public List<Collider> myColliders;
    Dictionary<int, List<float4>> hittionary = new Dictionary<int, List<float4>>();

    internal int hitCount;

    private IEnumerator Start()
    {
        if (myColliders.Count == 0)
        {
            Collider c = this.GetComponent<Collider>();
            if (c) myColliders.Add(c);
        }
        foreach (var item in myColliders)
        {
            SplatManager.Instance.paintColliderDictionary.Add(item, this);
        }
        StartCoroutine(ManageDrips());
        yield return new WaitForSeconds(0.5f); ;
        SplatManager.Instance.RegisterPaintWorker(this);
    }
    void ResetHittionary()
    {
        hitCount = 0;
        for (int i = 0; i < 8; i++)
        {
            if (hittionary.ContainsKey(i))
            {
                hittionary[i].Clear();
            }
            else
            {
                hittionary.Add(i,new List<float4>());
            }
        }
    }
    public void GetHit(HitData hd)
    {
        float4 point;
        Vector3 hitPointLocal = this.transform.InverseTransformPoint(hd.hitPoint);
        point.x = hitPointLocal.x;
        point.y = hitPointLocal.y;
        point.z = hitPointLocal.z;
        point.w = hd.radius * lengthMult;
        //point.w = this.transform.InverseTransformVector(hd.radius * Vector3.up).magnitude;
        //Debug.Log(point.w);
        hittionary[hd.colorIndex].Add(point);
        hitCount++;

    }
    public void AddDrip(DripData dd)
    {
        dd.CopyInitialRadius();
        drips.Add(dd);
    }

    public List<DripData> drips = new List<DripData>();

    IEnumerator ManageDrips()
    {
        float v;
        DripData drip;
        while (true)
        {
            for (int i = drips.Count-1; i>=0 ; i--)
            {

                drip = drips[i];
                if (drip.sprintTimeBudget > 0)
                {
                    drip.sprintTimeBudget -= Time.deltaTime;
                }
                else
                {
                    drip.dropSpeed -= drip.decay * Time.deltaTime;
                }
                v = drip.dropSpeed;
                if (v < MIN_DRIP_SPEED)
                {
                    drips.RemoveAt(i);
                    continue;
                }
                drip.rcPoint = drip.rcPoint + Vector3.down * v * Time.deltaTime;

                RaycastHit rch;
                if (Physics.Raycast(drip.rcPoint, drip.rcDir, out rch, 100, SplatManager.Instance.layerMask))
                {
                    if (rch.collider.gameObject == this.gameObject)
                    {
                        HitData hd = new HitData();
                        hd.radius = drip.radius;
                        hd.hitPoint = rch.point;
                        hd.colorIndex = drip.colorIndex;
                        GetHit(hd);
                    }
                    else
                    {
                        Debug.LogError("Unknown object");
                        drips.RemoveAt(i);
                        continue;
                    }
                }
                else
                {
                    drips.RemoveAt(i);
                    continue;
                }


                //v = (drip.radius) * drip.dropSpeed;
                //if (v < MIN_DRIP_SPEED)
                //{
                //    drips.RemoveAt(i);
                //    continue;
                //}
                //else
                //{
                //    drip.radius = drip.radius - drip.decay * Time.deltaTime;
                //    drip.rcPoint = drip.rcPoint + Vector3.down * v*Time.deltaTime;

                //    RaycastHit rch;
                //    if (Physics.Raycast(drip.rcPoint, drip.rcDir,out rch, 100, SplatManager.Instance.layerMask))
                //    {
                //        if (rch.collider.gameObject == this.gameObject)
                //        {
                //            HitData hd = new HitData();
                //            hd.radius = drip.radius;
                //            hd.hitPoint = rch.point;
                //            hd.colorIndex = drip.colorIndex;
                //            GetHit(hd);
                //        }
                //        else
                //        {
                //            Debug.LogError("Unknown object");
                //            drips.RemoveAt(i);
                //            continue;
                //        }
                //    }
                //    else
                //    {
                //        drips.RemoveAt(i);
                //        continue;
                //    }
                //}
            }
            yield return null;
        }
    }

    const float MAX_DRIP_SPEED = 1;
    const float MIN_DRIP_SPEED = 0.05f;
}

public struct HitData
{
    public Vector3 hitPoint;
    public float radius;
    public int colorIndex;

}
[System.Serializable]
public class DripData
{
    public Vector3 rcPoint;
    public Vector3 rcDir;
    public float radius;
    public int colorIndex;
    public float initialRadius { get; private set; }
    public float dropSpeed;
    public float decay;
    public float sprintTimeBudget;
    public void CopyInitialRadius()
    {
        initialRadius = radius;
    }
}