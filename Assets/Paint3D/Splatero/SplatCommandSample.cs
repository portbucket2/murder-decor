﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SplatCommandSample : MonoBehaviour
{
    public float radius = 1;
    public List<Button> buttonsToChange;

    private Camera cam;
    private int colorIndex;
    private int colorCount;
    void Start()
    {
        cam = Camera.main;
        colorCount = SplatManager.Instance.cols.Count;

        for (int i = 0; i < SplatManager.Instance.cols.Count; i++)
        {
            int index = i;
            buttonsToChange[i].GetComponent<Image>().color = SplatManager.Instance.cols[i];
            buttonsToChange[i].onClick.AddListener(() => { colorIndex = index; });
        }
    }

    void Update()
    {

        for (int i = 0; i < 8; i++)
        {
            if (Input.GetKeyDown((KeyCode)(i + 256)))
            {
                colorIndex = i;
            }
        }


        if (Input.GetMouseButtonDown(0))
        {
            Vector3 v = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
            Ray r = cam.ScreenPointToRay(v);
            RaycastHit rch;
            if (Physics.Raycast(r,out rch, 100, SplatManager.Instance.layerMask))
            {
                Vector3 rdir = r.direction.normalized;
                //PaintJobManager.SplatNow(colorIndex, r, 0.5f, 4, 2.5f);
                SplatManager.SplatBurst(
                    colorIndex: colorIndex,
                    ray: new Ray(rch.point-rdir*0.2f, rdir),
                    baseRadius: 0.5f,
                    splatCount: 4,
                    spreadFactor: 2.5f);
            }

        }
    }
}
