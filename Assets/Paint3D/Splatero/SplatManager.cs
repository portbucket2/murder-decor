﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Mathematics;
using FRIA;

public class SplatManager : MonoBehaviour
{
    //public float spreadFactor = 1.0f;
    public LayerMask layerMask;
    public GameObject splatParticlePrefab;
    public static SplatManager Instance { get;private set; }
    public List<Color> cols;
    private List<SplatTargetJobController> paintJobCons = new List<SplatTargetJobController>();

    private List<SplatTargetJobController> scheduled = new List<SplatTargetJobController>();

    internal Dictionary<Collider, SplatTargetJobController> paintColliderDictionary = new Dictionary<Collider, SplatTargetJobController>();

    public List<ParticleSystemRenderer> splatColoredParticlePrefabs;
    void Awake()
    {
        Instance = this;

        Debug.Log(string.Format("{0} - {1}", name, Time.time));
        if (splatParticlePrefab)
        {
            foreach (var currentColor in cols)
            {
                GameObject go = Instantiate(splatParticlePrefab, new Vector3(0, -50, 0), quaternion.identity);
                go.SetActive(false);
                ParticleSystemRenderer psmr = go.GetComponent<ParticleSystemRenderer>();
                psmr.material = Instantiate(psmr.material);
                splatColoredParticlePrefabs.Add(psmr);
            }
        }
    }

    public void ColorLoading(List<Color> colors) {

        int numberOfColor = colors.Count;
        cols.Clear();
        if (splatParticlePrefab)
        {
            for (int i = 0; i < numberOfColor; i++)
            {

                cols.Add(colors[i]);
                splatColoredParticlePrefabs[i].material.SetColor("_BaseColor", colors[i]);
            }
        }
    }

    public void RegisterPaintWorker(SplatTargetJobController pjc)
    {
        paintJobCons.Add(pjc);
        pjc.Init(cols);


    }


    public float[] progressList = new float[8];
    void Update()
    {

        for (int i = scheduled.Count - 1; i >= 0; i--)
        {
            if (scheduled[i].totalHandle.IsCompleted)
            {
                scheduled[i].totalHandle.Complete();
                scheduled[i].ApplyJobResult();
                scheduled.RemoveAt(i);
            }
        }
        for (int c = 0; c < 8; c++)
        {
            progressList[c] = 0;
        }

        float totalWeight=0;
        for (int i = 0; i < paintJobCons.Count; i++)
        {
            SplatTargetJobController pjc = paintJobCons[i];
            if (pjc.calculateProgressUpTo_percentage > 0)
            {
                totalWeight += pjc.calculateProgressUpTo_percentage * pjc.N * pjc.N/100;
            }
        }
        float totalProgress = 0;
        for (int i = 0; i < paintJobCons.Count; i++)
        {
            SplatTargetJobController pjc = paintJobCons[i];
            if (pjc.calculateProgressUpTo_percentage > 0)
            {
                for (int c = 0; c < 8; c++)
                {
                    totalProgress += pjc.pixProgressArray[c];
                    progressList[c] += pjc.pixProgressArray[c];
                }
            }
        }
        for (int c = 0; c < 8; c++)
        {
            progressList[c] *= 100/(((totalProgress>totalWeight)?totalProgress:totalWeight));
        }


        for (int i = 0; i < paintJobCons.Count; i++)
        {
            SplatTargetJobController pjc = paintJobCons[i];
            if (pjc.hitCount > 0)
            {
                int index = scheduled.IndexOf(pjc);
                if (index >= 0)
                {
                    pjc.totalHandle.Complete();
                    pjc.ApplyJobResult();
                    scheduled.RemoveAt(index);
                }
                pjc.ScheduleRecolor();
                scheduled.Add(pjc);
            }
        }
    }


    public const float MAX_RAY_CAST_DISTANCE = 100;
    static RaycastHit[] rchs;
    public static void SplatBurst_WithParticleManagement(int colorIndex, Ray ray, float baseRadius, int splatCount = 4, float spreadFactor = 2.5f, float splatDelay = 0.25f)
    {
        GameObject pgo = null;
        if (Instance.splatParticlePrefab)
        {
            pgo = Pool.Instantiate(Instance.splatColoredParticlePrefabs[colorIndex].gameObject, ray.origin, quaternion.identity);
            pgo.SetActive(true);
            ParticleSystem p = pgo.GetComponent<ParticleSystem>();
            p.Stop();
            p.Play();
        }
        Centralizer.Add_DelayedMonoAct(Instance, () => {
            if (pgo) Centralizer.Add_DelayedMonoAct(Instance, () => { Pool.Destroy(pgo); }, 4);
            SplatBurst(colorIndex, ray, baseRadius, splatCount, spreadFactor);
        }, splatDelay);
    }

    public static void SplatBurst(int colorIndex, Ray ray, float baseRadius, int splatCount = 4, float spreadFactor = 2.5f)
    {
        Vector3 origin = ray.origin;
        Vector3 dir = ray.direction;
        Vector3 crossAgainst = Vector3.up;
        float angle = Vector3.Angle(dir, crossAgainst);
        if (angle > 1 && angle < 179)
        {
            crossAgainst = Vector3.right;
        }
        Vector3 crossX = Vector3.Cross(dir, crossAgainst).normalized;
        Vector3 crossY = Vector3.Cross(dir, crossX).normalized;

        int hitCount = 0;
        for (int i = 0; i < splatCount; i++)
        {
            int index = i;
            float personalRadius = baseRadius;
            if (splatCount != 1) personalRadius = baseRadius * (1 - (0.45f * index / (splatCount - 1)));
            float personalDelay = index * .10f / splatCount;

            float f = (i / (float)splatCount);
            float offsetBaseDist = baseRadius * Mathf.Pow(f, 0.5f) * spreadFactor;// *Handy.Deviate(1,0.25f);
            Vector3 offsetDir = (crossX * UnityEngine.Random.Range(-1, 1.0f) + crossY * UnityEngine.Random.Range(-1, 1.0f));
            Vector3 originI = origin + offsetDir.normalized * offsetBaseDist;

            rchs = Physics.RaycastAll(originI, dir, baseRadius * 2, Instance.layerMask);

            for (int h = 0; h < rchs.Length; h++)
            {
                RaycastHit rch = rchs[h];
                int h2 = h;
                //Debug.DrawLine(originI, rch.point, Color.yellow, 1);

                if (Instance.paintColliderDictionary.ContainsKey(rch.collider))
                {
                    Centralizer.Add_DelayedMonoAct(Instance, () =>
                    {
                        hitCount++;

                        SplatTargetJobController pjc = Instance.paintColliderDictionary[rch.collider];

                        HitData hd = new HitData();
                        hd.hitPoint = rch.point;
                        hd.radius = personalRadius;
                        hd.colorIndex = colorIndex;
                        pjc.GetHit(hd);

                        DripData dd = new DripData();
                        dd.rcPoint = originI;
                        dd.rcDir = dir;
                        dd.radius = personalRadius/2;
                        dd.colorIndex = colorIndex;
                        dd.sprintTimeBudget = 1;
                        dd.decay = .75f;
                        dd.dropSpeed = .25f;

                        pjc.AddDrip(dd);

                    }, personalDelay);

                }

            }
        }
        if (hitCount == 0)
        {
            Debug.DrawRay(origin, crossX * 1f, Color.green, 1);
            Debug.DrawRay(origin, crossY * 1f, Color.blue, 1);
            Debug.DrawRay(origin, dir * .5f, Color.red, 1);
        }
    }
}
