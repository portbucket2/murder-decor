﻿using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;


[BurstCompile]
public struct PaintJob : IJobParallelFor
{

    [ReadOnly] public NativeArray<int> params_int;
    [ReadOnly] public NativeArray<float> params_float;
    //public NativeArray<float3> verts;
    [ReadOnly] public NativeArray<float2> uvData;

    [ReadOnly] public NativeArray<float2> indicesInRange;


    [NativeDisableContainerSafetyRestriction] public NativeArray<float4> splatArray0;
    [NativeDisableContainerSafetyRestriction] public NativeArray<float4> splatArray1;
    void IJobParallelFor.Execute(int r)
    {
        int indicesInRangeCount = params_int[3];
        if (indicesInRangeCount > 0)
        {
            int colorCount = 8;
            int colorIndex = params_int[1];
            int N = params_int[2];
            //float count1D = math.sqrt(indicesInRangeCount * 4 / math.PI);

            float worldToPix = params_float[0];
            float innerRatio = params_float[1];

            float pixRadius;
            float pixRadiusInner;
            float pixRadOut;

            float pixRadSqr;
            float pixRadSq_Inner;

            int u, v;
            float sqrPixDist, pixDist;
            float spacingMult, prevTotal, oldValI;
            float r_io;
            float2 uvd;

            NativeArray<float> pixVals = new NativeArray<float>(8, Allocator.Temp);
            int pixIndex = 0;
            bool skipPixel;
            float targetPixVal;
            float4 color0;
            float4 color1;
            float2 currentIndexData;
            for (int c = 0; c < N; c++)
            {
                pixIndex = r * N + c;
                color0 = splatArray0[pixIndex];
                color1 = splatArray1[pixIndex];

                pixVals[0] = color0.x;
                pixVals[1] = color0.y;
                pixVals[2] = color0.z;
                pixVals[3] = color0.w;

                pixVals[4] = color1.x;
                pixVals[5] = color1.y;
                pixVals[6] = color1.z;
                pixVals[7] = color1.w;


                skipPixel = true;
                targetPixVal = pixVals[colorIndex];

                int temp_i;
                for (int ind_i = 0; ind_i < indicesInRangeCount; ind_i++)
                {
                    currentIndexData = indicesInRange[ind_i];

                    pixRadius = worldToPix * currentIndexData.y;
                    pixRadiusInner = innerRatio * pixRadius;
                    pixRadOut = (pixRadius - pixRadiusInner);

                    pixRadSqr = pixRadius * pixRadius;
                    pixRadSq_Inner = pixRadiusInner * pixRadiusInner;

                    temp_i = (int)indicesInRange[ind_i].x;
                    uvd = uvData[temp_i];
                    u = (int)(uvd.x * N);
                    v = (int)(uvd.y * N);

                    sqrPixDist = (u - c) * (u - c) + (v - r) * (v - r);

                    if (sqrPixDist < pixRadSq_Inner)
                    {
                        r_io = 1;
                        skipPixel = false;
                    }
                    else if (sqrPixDist < pixRadSqr)
                    {
                        pixDist = math.sqrt((u - c) * (u - c) + (v - r) * (v - r));
                        r_io = 1 - ((pixDist - pixRadiusInner) / pixRadOut);
                        skipPixel = false;
                    }
                    else
                    {
                        r_io = 0;
                    }

                    if (r_io > targetPixVal)
                    {
                        targetPixVal = r_io;
                    }
                }


                if (!skipPixel)
                {


                    if (pixVals[colorIndex] < targetPixVal)
                    {
                        if (targetPixVal >= 1)
                        {
                            for (int ci = 0; ci < colorCount; ci++)
                            {
                                pixVals[ci] = 0;
                            }
                            pixVals[colorIndex] = 1;
                        }
                        else
                        {
                            prevTotal = 0;
                            oldValI = pixVals[colorIndex];
                            for (int ci = 0; ci < colorCount; ci++)
                            {
                                prevTotal += pixVals[ci];
                            }
                            prevTotal -= oldValI;

                            spacingMult = 1 - targetPixVal;
                            if (prevTotal > 0)
                            {
                                for (int ci = 0; ci < colorCount; ci++)
                                {
                                    pixVals[ci] = pixVals[ci] * spacingMult / prevTotal;
                                }
                            }
                            pixVals[colorIndex] = targetPixVal;
                        }
                    }
                }


                color0.x = pixVals[0];
                color0.y = pixVals[1];
                color0.z = pixVals[2];
                color0.w = pixVals[3];

                color1.x = pixVals[4];
                color1.y = pixVals[5];
                color1.z = pixVals[6];
                color1.w = pixVals[7];

                splatArray0[pixIndex] = color0;
                splatArray1[pixIndex] = color1;

            }

            pixVals.Dispose();
        }


        //params_int[1]++;




    }
}


[BurstCompile]
public struct IncrementIndexJob : IJob
{
    public NativeArray<int> params_int;

    void IJob.Execute()
    {
        params_int[1]++;
    }
}