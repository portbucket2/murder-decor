﻿using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;


[BurstCompile]
public struct SearchVertJob : IJob
{
    [ReadOnly] public NativeArray<float3> verts;
    public NativeArray<float2> indicesInRange;
    public NativeArray<int> params_int;
    [ReadOnly] public NativeArray<float> params_float;
    [ReadOnly] public NativeArray<float4> paintPoints;
    void IJob.Execute()
    {
        int indicesInRangeCount = 0;


        if (paintPoints.Length > 0)
        {
            float lengthMult = params_float[10];
            float x, y, z, sqrMag;
            float r;
            float3 v;
            float4 paintPoint;
            bool alreadyAdded;
            float2 vals;
            int closestVertIndex=0;//when 0 verts found;
            float closestSqrMag = float.MaxValue;

            float areaOffset = 0;

            NativeArray<int2> tempint2 = new NativeArray<int2>(paintPoints.Length, Allocator.Temp);
            for (int i = 0; i < verts.Length; i++)
            {

                v = verts[i];

                alreadyAdded = false;
                vals.x = 0;
                vals.y = 0;
                for (int p = 0; p < paintPoints.Length; p++)
                {
                    paintPoint = paintPoints[p];
                    r = paintPoint.w;


                    x = v.x - paintPoint.x;
                    y = v.y - paintPoint.y;
                    z = v.z - paintPoint.z;

                    sqrMag = x * x + y * y + z * z;
                    if (sqrMag < r * r + areaOffset)
                    {
                        if (!alreadyAdded)
                        {

                            vals.x = i;
                            vals.y = r * r;
                            indicesInRange[indicesInRangeCount] = vals;
                            alreadyAdded = true; 
                            int2 temp = tempint2[p];
                            temp.y++;
                            tempint2[p] = temp;
                        }
                        else
                        {

                            //UnityEngine.Debug.LogError("strangerBranch");
                            vals.y += r * r;
                            indicesInRange[indicesInRangeCount] = vals;
                        }
                        int2 temp2 = tempint2[p];
                        temp2.x++;
                        tempint2[p]= temp2;
                    }
                    if (p == 0)
                    {
                        if (sqrMag < closestSqrMag)
                        {
                            closestSqrMag = sqrMag;
                            closestVertIndex = i;
                        }
                    }

                }

                if (alreadyAdded)
                {
                    vals.y = math.sqrt(vals.y) /lengthMult ;
                    indicesInRange[indicesInRangeCount] = vals;
                    indicesInRangeCount++;

                }
            }
            for (int i = 0; i < tempint2.Length; i++)
            {
                //UnityEngine.Debug.LogFormat("{0}",tempint2[i]);
            }
            tempint2.Dispose(); ;
            if (indicesInRangeCount <= 0)
            {
                //UnityEngine.Debug.LogFormat("{0}===={1}", paintPoints[0],verts[closestVertIndex]);
                vals.x = closestVertIndex;
                vals.y = paintPoints[0].w /lengthMult ;
                indicesInRange[0] = vals;
                indicesInRangeCount = 1;

            }
        }
        //UnityEngine.Debug.LogError(paintPoints.Length);
        params_int[3] = indicesInRangeCount;
    }
}
