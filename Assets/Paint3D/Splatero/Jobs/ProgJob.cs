﻿using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;


[BurstCompile]
public struct ProgJob : IJob
{

    [ReadOnly] public NativeArray<int> params_int;
    [ReadOnly] public NativeArray<float> params_float;
     public NativeArray<float> vals;


    [ReadOnly] public NativeArray<float4> splatArray0;
    [ReadOnly] public NativeArray<float4> splatArray1;
    void IJob.Execute()
    {
        int N = params_int[2];
        float4 c1 = new float4(0, 0, 0, 0);
        float4 c2 = new float4(0, 0, 0, 0);
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                c1 += splatArray0[i * N + j];
                c2 += splatArray1[i * N + j];
            }
        }
        vals[0] = c1.x;
        vals[1] = c1.y;
        vals[2] = c1.z;
        vals[3] = c1.w;

        vals[4] = c2.x;
        vals[5] = c2.y;
        vals[6] = c2.z;
        vals[7] = c2.w;

        //float sum = 0;
        //for (int i = 0; i < 8; i++)
        //{
        //    sum += vals[i];
        //}
        //float max = params_float[15];
        //if (sum > max) max = sum;

        //for (int i = 0; i < 8; i++)
        //{
        //   vals[i] /=max;
        //}

    }
}
